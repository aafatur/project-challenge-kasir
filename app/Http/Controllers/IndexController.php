<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function dashboard()
    {
        $data['title'] = "Halaman Dashboard";
    
        return view('home',$data);
    }
}
